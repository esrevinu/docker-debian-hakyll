FROM debian:unstable-slim

ENV LANG=C.UTF-8
ENV PATH=/root/.cabal/bin:$PATH
RUN apt-get update && apt-get install -y libghc-hakyll-dev \
    libghc-lens-dev cabal-install make g++
RUN cabal v2-update
RUN mkdir -p /root/.cabal/store/ghc-8.6.5/package.db
RUN cabal v2-install cabal-install
RUN apt-get remove -y cabal-install && apt -y autoremove
ADD ./cabal_config /root/.cabal/config
RUN cabal install --ghc-option=-dynamic --lib hakyll-sass
RUN rm -rf /root/.cabal/packages/hackage.haskell.org/01-index.tar.gz
RUN find /root/.cabal/packages/hackage.haskell.org/ -type d \
    -maxdepth 1 -mindepth 1 -exec rm -rf {} \;
RUN rm -rf /var/lib/apt/lists/*

FROM scratch
COPY --from=0 / /
ENV LANG=C.UTF-8 PATH=/root/.cabal/bin:$PATH
CMD ["/bin/bash"]
